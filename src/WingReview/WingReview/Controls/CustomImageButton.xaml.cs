﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using WingReview.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WingReview.Controls
{
	/// <summary>
	/// 既定のImageButtonでは、Imageによる描画がデバイスごとに異なるため
	/// デバイスで共通の挙動を実現するImageButtonを実現する
	/// </summary>
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CustomImageButton : ContentView
	{
		public CustomImageButton ()
		{
			InitializeComponent ();
		}

		/// <summary>
		/// 対象のImageのファイル名
		/// </summary>
		public string Source { get; set; }

		/// <summary>
		/// クリックイベント
		/// </summary>
		public event System.EventHandler Clicked;

		/// <summary>
		/// ボタンの操作を実行中か
		/// </summary>
		public bool Executing
		{
			get => this.IsEnabled;
			set
			{
				IsEnabled = value;

				// 実行中はボタンを簡易的にグレー表示する
				BoxView.IsVisible = !value;
			}
		}

		/// <summary>
		/// ボタンクリックハンドラ
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private async void MainButton_OnClicked(object sender, EventArgs e)
		{
			// クリック時に一瞬大きくなって元に戻る
			this.Scale = 1.4;
			await this.RelScaleTo(-0.4, 100);

			// イベント発行
			Clicked?.Invoke(this, e);
		}

		/// <summary>
		/// 背景描画ハンドラ
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void CanvasView_OnPaintSurface(object sender, SKPaintSurfaceEventArgs e)
		{
			SKImageInfo info = e.Info;
			SKSurface surface = e.Surface;
			SKCanvas canvas = surface.Canvas;

			DrawBitmap(canvas);
		}

		/// <summary>
		/// 画像の描画
		/// </summary>
		/// <param name="canvas"></param>
		private void DrawBitmap(SKCanvas canvas)
		{
			canvas.Clear();

			// 画像の読み込み
			string resourceId = $"WingReview.Resources.Images.{Source}";
			var bitmap = SKBitmapFactory.Instance.GetSKBitmap(resourceId);

			// 画像の描画
			canvas.DrawBitmap(bitmap, 0, 0);
		}

	}
}