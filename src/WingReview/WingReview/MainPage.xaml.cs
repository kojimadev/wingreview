﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using WingReview.Extensions;
using WingReview.TouchTracking;
using WingReview.Utilities;
using Xamarin.Forms;

namespace WingReview
{
	public partial class MainPage : ContentPage
	{
		#region フィールド

		/// <summary>
		/// Pathの描画に利用するPaint
		/// </summary>
		private readonly SKPaint _PathPaint = new SKPaint
		{
			Style = SKPaintStyle.Stroke,
			Color = Color.FromHex("#D32F2F").ToSKColor(),
			StrokeWidth = 10,
			StrokeCap = SKStrokeCap.Round,
			StrokeJoin = SKStrokeJoin.Round
		};

		/// <summary>
		/// Pathの描画をクリアさせるか(TrueにすることでPathをクリアし、クリア後に自動的にfalseに戻す)
		/// </summary>
		private bool _ClearCanvas = false;

		/// <summary>
		/// 対象のbitmap
		/// </summary>
		SKBitmap _SKBitmap = null;

		/// <summary>
		/// 写真の倍率
		/// </summary>
		private float _Scale = 1;

		/// <summary>
		/// 写真表示時の左端からの余白
		/// </summary>
		private float _OffsetX = 0;

		/// <summary>
		/// 写真表示時の上端からの余白
		/// </summary>
		private float _OffsetY = 0;

		/// <summary>
		/// 描画中のPath
		/// </summary>
		private readonly Dictionary<long, SKPath> _InProgressPaths = new Dictionary<long, SKPath>();

		/// <summary>
		/// 描画完了したPath
		/// </summary>
		private readonly List<SKPath> _CompletedPaths = new List<SKPath>();

		#endregion

		#region 構築

		/// <summary>
		/// コンストラクタ
		/// </summary>
		public MainPage()
		{
			InitializeComponent();

			// 無駄なナビゲーションバーを非表示にする
			NavigationPage.SetHasNavigationBar(this, false);
		}

		#endregion

		#region イベントハンドラ

		private async void CameraButton_OnClicked(object sender, EventArgs e)
		{
			CameraButton.Executing = false;
			try
			{
				await SetImage(true);
			}
			finally
			{
				CameraButton.Executing = true;
			}
		}

		private async void PickButton_OnClicked(object sender, EventArgs e)
		{
			PickButton.Executing = false;
			try
			{
				await SetImage(false);
			}
			finally
			{
				PickButton.Executing = true;
			}
		}

		private async void MailButton_OnClicked(object sender, EventArgs e)
		{
			MailButton.Executing = false;
			try
			{
				await SendMail();
			}
			finally
			{
				MailButton.Executing = true;
			}
		}

		private void PrivacyButton_OnClicked(object sender, EventArgs e)
		{
			Device.OpenUri(new Uri("https://sites.google.com/view/wing-review/license"));
			Device.OpenUri(new Uri("https://sites.google.com/view/wing-review/privacy-policy"));
		}

		/// <summary>
		/// キャンバスの描画ハンドラ
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		private void CanvasView_OnPaintSurface(object sender, SKPaintSurfaceEventArgs args)
		{
			SKImageInfo info = args.Info;
			SKSurface surface = args.Surface;
			SKCanvas canvas = surface.Canvas;
			canvas.Clear();

			UpdateCanvas(info, canvas);
		}

		#endregion

		#region 内部処理

		/// <summary>
		/// メールの送信
		/// </summary>
		/// <returns></returns>
		private async Task SendMail()
		{
			// bitmapが無ければ何もしない
			if (_SKBitmap == null)
			{
				return;
			}

			SKEncodedImageFormat imageFormat = SKEncodedImageFormat.Jpeg;
			int quality = 70;

			using (MemoryStream memStream = new MemoryStream())
			using (SKManagedWStream wStream = new SKManagedWStream(memStream))
			{
				// JPGファイルを作成（PNGよりJPGの方がEncodeが速い）
				// 時間がかかる場合があるため、ボタンを無効状態に更新するためにawait
				await Task.Run(() => _SKBitmap.Encode(wStream, imageFormat, quality));
				byte[] data = memStream.ToArray();
				string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "review.JPG");
				File.WriteAllBytes(filePath, data);

				// 添付ファイルとしてメール送信
				ShareService service = new ShareService();
				await service.ShareAttachment(filePath);
			}
		}

		/// <summary>
		/// 対象の写真を画面に設定する
		/// </summary>
		/// <param name="takePhoto">true：写真を撮る、false：写真の中から選択</param>
		private async Task SetImage(bool takePhoto)
		{
			// 権限チェック
			var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
			var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
			if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
			{
				// 権限の要求
				var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
				cameraStatus = results[Permission.Camera];
				storageStatus = results[Permission.Storage];
			}
			// 再び権限チェック
			if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
			{
				await DisplayAlert("カメラとストレージの許可", "アプリケーションにカメラとストレージの使用を許可してください。", "OK");
				return;
			}

			// Plugin の初期化
			await Plugin.Media.CrossMedia.Current.Initialize();

			// カメラが使用可能で、写真が撮影可能かを判定
			if (!Plugin.Media.CrossMedia.Current.IsCameraAvailable || !Plugin.Media.CrossMedia.Current.IsTakePhotoSupported)
			{
				// そもそもカメラが使用不可または写真が撮影不可の場合、終了
				return;
			}

			MediaFile file = null;
			if (takePhoto)
			{
				// カメラを起動し写真を撮影する。撮影した写真はストレージに保存され、ファイルの情報が return される
				file = await Plugin.Media.CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
				{
					// ストレージに保存するファイル情報
					// すでに同名ファイルがある場合は、temp_1.jpg などの様に連番がつけられ名前の衝突が回避される
					Directory = "TempPhotos",
					Name = "temp.jpg",
					AllowCropping = false,
				});
			}
			else
			{
				// ギャラリーから写真を選択する
				file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync();
			}

			// 端末でユーザーが「キャンセル」した場合は、file が null となる
			if (file == null)
			{
				return;
			}

			// Bitmapの作成
			_SKBitmap = SKBitmap.Decode(new SKFileStream(file.Path));

			// bitmapを作成できた場合に限り、メール送信ボタンを表示
			MailButton.IsVisible = true;

			// MediaPluginから取得したファイルは削除しておく
			System.IO.File.Delete(file.Path);
			file.Dispose();

			// いったん描画パスをクリアして描画用キャンバスに写真を描画
			_ClearCanvas = true;
			CanvasView.InvalidateSurface();
		}

		/// <summary>
		/// キャンバスの更新
		/// </summary>
		/// <param name="info"></param>
		/// <param name="canvas"></param>
		private void UpdateCanvas(SKImageInfo info, SKCanvas canvas)
		{
			// クリアする意図で描画更新した場合か
			if (_ClearCanvas)
			{
				_ClearCanvas = false;
				_CompletedPaths.Clear();
				_InProgressPaths.Clear();
			}

			// bitmapがなければ、何もしない
			if (_SKBitmap == null)
			{
				return;
			}

			// bitmapにPathを書き込む
			using (SKCanvas bitmapCanvas = new SKCanvas(_SKBitmap))
			{
				foreach (SKPath path in _CompletedPaths)
				{
					bitmapCanvas.DrawPath(path, _PathPaint);
				}

				foreach (SKPath path in _InProgressPaths.Values)
				{
					bitmapCanvas.DrawPath(path, _PathPaint);
				}
			}

			// 写真の倍率を縦横比を維持するように設定
			_Scale = Math.Min((float)info.Width / _SKBitmap.Width,
				(float)info.Height / _SKBitmap.Height);

			// 左端および上端からの余白の算出
			float x = (info.Width - _Scale * _SKBitmap.Width) / 2;
			float y = (info.Height - _Scale * _SKBitmap.Height) / 2;
			_OffsetX = (float)(CanvasView.CanvasSize.Width * x / CanvasView.Width / _Scale);
			_OffsetY = (float)(CanvasView.CanvasSize.Height * y / CanvasView.Height / _Scale);

			// bitmapの出力位置を設定してcanvasに出力
			SKRect destRect = new SKRect(x, y, x + _Scale * _SKBitmap.Width,
				y + _Scale * _SKBitmap.Height);
			canvas.DrawBitmap(_SKBitmap, destRect);
		}

		#endregion

		#region Pathの描画

		// 以下の記事のサンプル参照
		// https://docs.microsoft.com/ja-jp/xamarin/xamarin-forms/user-interface/graphics/skiasharp/paths/finger-paint
		// TouchEffectの詳細は以下参照
		// https://docs.microsoft.com/ja-jp/xamarin/xamarin-forms/app-fundamentals/effects/touch-tracking

		private void TouchEffect_OnTouchAction(object sender, TouchActionEventArgs args)
		{
			switch (args.Type)
			{
				case TouchActionType.Pressed:
					if (!_InProgressPaths.ContainsKey(args.Id))
					{
						SKPath path = new SKPath();
						path.MoveTo(ConvertToPixel(args.Location));
						_InProgressPaths.Add(args.Id, path);
						CanvasView.InvalidateSurface();
					}
					break;

				case TouchActionType.Moved:
					if (_InProgressPaths.ContainsKey(args.Id))
					{
						SKPath path = _InProgressPaths[args.Id];
						path.LineTo(ConvertToPixel(args.Location));
						CanvasView.InvalidateSurface();
					}
					break;

				case TouchActionType.Released:
					if (_InProgressPaths.ContainsKey(args.Id))
					{
						_CompletedPaths.Add(_InProgressPaths[args.Id]);
						_InProgressPaths.Remove(args.Id);
						CanvasView.InvalidateSurface();
					}
					break;

				case TouchActionType.Cancelled:
					if (_InProgressPaths.ContainsKey(args.Id))
					{
						_InProgressPaths.Remove(args.Id);
						CanvasView.InvalidateSurface();
					}
					break;
			}
		}

		private SKPoint ConvertToPixel(Point pt)
		{
			return new SKPoint((float)(CanvasView.CanvasSize.Width * pt.X / CanvasView.Width / _Scale - _OffsetX),
				(float)(CanvasView.CanvasSize.Height * pt.Y / CanvasView.Height / _Scale - _OffsetY));
		}

		#endregion

	}
}
