﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using SkiaSharp;

namespace WingReview.Utilities
{
	/// <summary>
	/// SKBitmapを返すクラス
	/// </summary>
	public class SKBitmapFactory
	{
		/// <summary>
		/// 画像ファイルのキャッシュ
		/// </summary>
		private static readonly Dictionary<string, SKBitmap> _BitmapDictionary = new Dictionary<string, SKBitmap>();

		/// <summary>
		/// 本クラスのインスタンスを返す
		/// </summary>
		public static SKBitmapFactory Instance => new SKBitmapFactory();

		/// <summary>
		/// 埋め込まれたリソースとしての画像ファイルを返す(一度作成したものはキャッシュする)
		/// </summary>
		/// <param name="resourceId"></param>
		/// <returns></returns>
		public SKBitmap GetSKBitmap(string resourceId)
		{
			if (_BitmapDictionary.ContainsKey(resourceId))
			{
				return _BitmapDictionary[resourceId];
			}

			Assembly assembly = GetType().GetTypeInfo().Assembly;
			using (Stream stream = assembly.GetManifestResourceStream(resourceId))
			{
				SKBitmap bitmap = SKBitmap.Decode(stream);
				_BitmapDictionary[resourceId] = bitmap;
				return bitmap;
			}
		}
	}
}
