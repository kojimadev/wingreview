﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace WingReview.Utilities
{
	/// <summary>
	/// Share機能を提供する
	/// </summary>
    public class ShareService
    {
		/// <summary>
		/// テキストをシェアする
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
	    public async Task ShareText(string text)
	    {
		    await Share.RequestAsync(new ShareTextRequest
		    {
			    Text = text,
			    Title = "Share Text"
		    });
	    }

		/// <summary>
		/// URLをシェアする
		/// </summary>
		/// <param name="uri"></param>
		/// <returns></returns>
	    public async Task ShareUri(string uri)
	    {
		    await Share.RequestAsync(new ShareTextRequest
		    {
			    Uri = uri,
			    Title = "Share Web Link"
		    });
	    }

		/// <summary>
		/// 添付ファイルをメール送信する
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
	    public async Task ShareAttachment(string filePath)
	    {
			var message = new EmailMessage
			{
				Subject = "Review Result",
				Body = string.Empty,
			};

			message.Attachments.Add(new EmailAttachment(filePath));

			await Email.ComposeAsync(message);
		}
	}
}
